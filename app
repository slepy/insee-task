#!/usr/bin/env php

<?php

/** app.php **/
require_once __DIR__ . '/vendor/autoload.php';

use App\GitRepository;
use Symfony\Component\Console\Application;

$app = new Application();
$app->add(new GitRepository());
$app->run();
