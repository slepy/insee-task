<?php

namespace App;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Formatter\OutputFormatterStyle;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\HttpClient\CurlHttpClient;

class GitRepository extends Command
{
    const COMMAND_NAME = 'get:sha';
    const USER_AGENT = 'INSEE';

    private $services = ['github.com'];

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Configuring Command
     *
     * @return void
     */
    public function configure(): void
    {
        $this->setName(self::COMMAND_NAME)
            ->addArgument('ownerRepo', InputArgument::REQUIRED, 'Owner and repository name of git repository')
            ->addArgument('branchName', InputArgument::REQUIRED, 'Branch name of git repository')
            ->addOption('service', 's', InputOption::VALUE_OPTIONAL, 'You can change service.');
    }

    /**
     * Executing Command
     *
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return void
     */
    public function execute(InputInterface $input, OutputInterface $output): void
    {
        $service = str_replace('=', '', $input->getOption('service') ?? $this->services[0]);

        if ($this->isServiceAvailable($service)){
            $sha = $this->getSha(
                $input->getArgument('ownerRepo'),
                $input->getArgument('branchName'),
                $service
            );

            $output->writeln($sha);
        } else {
            $outputStyle = new OutputFormatterStyle('red');
            $output->getFormatter()->setStyle('red', $outputStyle);

            $output->writeln('Unknown service <red>'. $service. '</>');
        }

    }
    /**
     * Check if servise is available
     *
     * @param string $service
     * @return boolean
     */
    private function isServiceAvailable(string $service): bool
    {
        return in_array($service, $this->services);
    }

    /**
     * Getting sha from git repository
     *
     * @param string $ownerRepo
     * @param string $branchName
     * @param string $service
     * @return string
     */
    private function getSha(string $ownerRepo, string $branchName, string $service = null): string
    {
        $url = 'https://api.'
            . $service
            . '/repos/'
            . $ownerRepo . '/'
            . 'branches/'
            . $branchName;

        $client = new CurlHttpClient(['headers' => [
            'User-Agent' => self::USER_AGENT,
        ]]);

        return $client->request('GET', $url)->toArray()['commit']['sha'];
    }
}
